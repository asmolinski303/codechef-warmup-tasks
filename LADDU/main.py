'''
input:
    TESTS AMOUNT
    [ACTIVITIES AMOUNT] [INDIAN/NON-INDIAN]
    activity [number]
    types of activities:
        TOP_CONTRIBUTOR
        BUG_FOUND [severity(from 50 to 1000 inclusive)]
        CONTEST_HOSTED
        CONTEST_WON [rank(from 1 to 5000 inclusive)]
'''

user_type = {"INDIAN","NON_INDIAN"}
activities = {"TOP_CONTRIBUTOR","BUG_FOUND","CONTEST_HOSTED","CONTEST_WON"} 
total_points = 0

def parse_line(line: str):
    result = line.split(" ")
    if len(result) == 2 or len(result) == 1:
        return result
    else:
        return None

def add_top_contributor():
    global total_points
    total_points += 300

def add_bug_report(severity):
    global total_points
    if severity >= 50 and severity <= 1000:
        total_points += severity

def add_contest_hosted():
    global total_points
    total_points += 50

def add_contest_won(rank):
    global total_points
    bonus = 20 - rank
    if bonus < 0:
        bonus = 0
    total_points = total_points + 300 + bonus


activity_function = dict.fromkeys(activities)

activity_function["TOP_CONTRIBUTOR"] = add_top_contributor
activity_function["CONTEST_HOSTED"] = add_contest_hosted
activity_function["CONTEST_WON"] = add_contest_won
activity_function["BUG_FOUND"] = add_bug_report

#runs from there >
tests = int(input())

for _ in range(tests):
    desc = parse_line(input())
    factors = int(desc[0])
    user_type = desc[1]
    for _ in range(factors):
        data = parse_line(input())
        if len(data) > 1:
            activity_function[data[0]](int(data[1]))
        else:
            activity_function[data[0]]()

    if user_type == "INDIAN":
        print(int(total_points/200))
    else:
        print(int(total_points/400))
        
    total_points = 0
