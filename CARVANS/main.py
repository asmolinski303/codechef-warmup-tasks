import sys

def get_velocities(line):

    return [int(velocity) for velocity in line.split(' ')]

def get_number_of_max_velocities_retained(velocities):

    velo_iter = iter(velocities)
    curr_velo = next(velo_iter)

    my_min = curr_velo
    counter = 1

    for velo in velo_iter:
        next_velo = velo
        
        if my_min >= next_velo:
            counter += 1
            my_min = next_velo

    return counter


if __name__ == '__main__':
    cases = int(input())

    for i in range(cases):
        cars = int(input())
        velocities_line = input()
        velocities = get_velocities(velocities_line)
        velocities = velocities[:cars]
        print(get_number_of_max_velocities_retained(velocities))

