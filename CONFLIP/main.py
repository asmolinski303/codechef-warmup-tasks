import typing
from typing import List


def get_game_array(start_side: int, size: int) -> List[str]:
   '''func returns list of H's or T's - that depends on start_side argument.
   number of letters is second argument'''
   if start_side == 1:
       return ['H' for x in range(size)]

   if start_side == 2:
       return ['T' for x in range(size)]

   return []

def flip_sides(array: List[str], flips_number: int) -> None:
    '''function flips H to T or otherwise, depending on initial state of element
    of given array'''
    if len(array) == 0:
        return
    
    sides_to_flip = 1
    while flips_number > 0:
        for i, e in enumerate(array):
            if i >= sides_to_flip:
                break

            array[i] = flip(array[i])
        
        #print(array)
        sides_to_flip += 1
        flips_number -= 1
        

def flip(char: str) -> str:
    if char == 'T':
        return 'H'
    elif char == 'H':
        return 'T'
    else:
        return

def count_chars(switch: int, array: List[str]) -> int:
    if switch == 1:
        return array.count('H')

    if switch == 2:
        return array.count('T')

    return



if __name__ == '__main__':
    tests = int(input())

    for i in range(tests):
        games = int(input())

        for j in range(games):
            desc_line = input()
            params = desc_line.split(' ')
            params  = [int(x) for x in params]

            if len(params) == 3:
                to_guess = params[2]
                array = get_game_array(params[0], params[1])
                flip_sides(array, params[1])
                print(count_chars(params[2], array))
            else:
                continue


