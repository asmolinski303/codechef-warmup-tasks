from typing import List

def get_revenues(budgets: List[int]) -> List[int]:
    'returns list of possible revenues from given budgets'
    count = len(budgets)
    budgets.sort() # in place
    result = dict.fromkeys(budgets)

    for budget in budgets:
        result[budget] = budget * count
        count -= 1 

    return result.values()

def get_revenues_using_list_comprahensions(budgets: List[int]) -> List[int]:
    budgets.sort(reverse=True)

    return [(place+1) * budget for place, budget in enumerate(budgets)]

if __name__ == '__main__':
    size = int(input())
    if size == 0:
        print(0)
    else:
        budgets = []

        for i in range(size):
            budgets.append(int(input()))

        #print(max(get_revenues(budgets)))
        print(max(get_revenues_using_list_comprahensions(budgets)))


