def get_number_of_trailing_zeros(num):
    'get number of trailing zeros'
    powered_five = 5
    counter = 0

    while num >= powered_five:
        counter += int(num/powered_five)

        powered_five = powered_five*5

    return counter

if __name__ == '__main__':
    cases = int(input())

    for i in range(cases):
        num = int(input())
        print(get_number_of_trailing_zeros(num))
