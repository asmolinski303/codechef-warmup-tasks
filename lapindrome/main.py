def check_lapindrome(my_str):
    halves = split_str(my_str)

    res1 = count_characters(halves[0])
    res2 = count_characters(halves[1])

    if res1 == res2:
        return True
    else:
        return False

def split_str(my_str):
    if len(my_str) % 2 == 0:
        return (my_str[0:int(len(my_str)/2)]
                , my_str[int(len(my_str)/2):int(len(my_str))])
    else:
        return (my_str[0:int(len(my_str)/2)]
                , my_str[int(len(my_str)/2+1):int(len(my_str))])

def count_characters(my_str):
    result = {}

    for c in my_str:
        if c in result:
            result[c] += 1
        else:
            result[c] = 1

    return result

if __name__ == '__main__':
    cases = int(input())

    for i in range(cases):
        if check_lapindrome(input()) is True:
            print('YES')
        else:
            print('NO')
