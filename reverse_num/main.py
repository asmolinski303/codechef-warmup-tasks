def reverse_num(num):
    num = int(num)
    if num < 10:
        return num
    if num > 1000000:
        return

    div = 10
    result = ''
    while num > 0:
        tmp = num % div
        result += str(tmp)
        num = int(num / 10)

    return int(result)

def reverse_str(my_str):
    if len(my_str) == 0:
        return

    if len(my_str) == 1:
        return my_str

    if my_str.endswith('0'):
        my_str = my_str.split('0')[0]

    result = ''

    for i, c in enumerate(my_str):
        result += my_str[len(my_str) - (i + 1)]

    return result



if __name__ == '__main__':
    cases = int(input())

    for i in range(cases):
        print(reverse_num(input()))
